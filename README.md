# Animated "Popdown" Menus

An exercise in DOM enter and exit animation with React.js.

This menu is a simple exercise in introducing and dismissing UI elements for ease of user experience.

To run: open the HTML file in any browser. In the corner, two menus will generate animated lists upon being clicked upon. Clicking outside the menus will disperse them.